const studentsArray = [ 'Богатов Иван', 'Вовк Максим', 'Кавун Ирина', 'Кравченко Иван', 'Мазур Аліна', 'Марченко Марія', 'Нагорняк Денис', 'Пампуха Леонід', 'Панасенко Артем', 'Полищук Андрей', 'Полікарпов Дмитро', 'Попов Максим', 'Розовик Ирина', 'Сеничак Назар', 'Терещенко Татьяна', 'Терновенко Андрій', 'Хаддад Иван', 'Цивінський Василь', 'Чорний Віталій', 'Шевкун Максим', 'Шевченко Микита', 'Щербина Варвара', 'Яценко Павло', 'Кирсенко Олексій'];

//T E S T  F E A T U R E S ! ! ! !
// const fillTheList = () => {
//     whatWeGotLocal.forEach((elem) => {
//         const studentList = document.querySelector('.list-of-students');
//         const studentName = document.createElement('p');
//         studentList.appendChild(studentName);
//         studentName.innerText = elem;
//         studentName.style.cssText = 'display : block;' +
//             'color : white;' +
//             'font-size : 14px;'
//     })
// };

//ПРОВЕРЯЕМ ЛОКАЛ СТОРДЖЖЖ НА НАЛИЧИЕ МАССИВА С ИМЕНАМИ В ВИДЕ СТРОКИ(ПО СУТИ ТРЕБУЕТСЯ ТОЛЬКО ПРИ ПЕРВОМ ЗАПУСКЕ)
const localCheck = () => {
  if(!localStorage.getItem('what-we-got'))  {
      localStorage.setItem('what-we-got', JSON.stringify(studentsArray));
  }
};

//ЗАПИСЫВАЕМ В ПЕРЕМЕННУЮ СТРОКУ ИЗ ЛОКАЛ СТОРЫДЖА В ВИДЕ МАССИВА
const whatWeGotLocal = JSON.parse(localStorage.getItem('what-we-got'));

console.log(whatWeGotLocal);

//ЭТА ПЕРЕМЕННАЯ БУДЕТ ХРАНИТЬБ В СЕБЕ, НА ВСЕХ ЭТАПАХ, ЗНАЧЕНИЕ ВЫБРАННОГО ЭЛЕМЕНТА МАССИВА
let randomNumb = 0;

//КНОПКА ВЫБОРА СТУДЕНТА
const startBtn = document.querySelector('.start-the-torture');
//ДИВ С КНОПКАМИ ПОДТВЕРЖДЕНИЯ
const booleenBtns = document.querySelector('.button-case');
//КНОПКА ПОДТВЕРЖДАЮЩЕЕ НАЛИЧИЕ СТУДЕНТА В АУДИТОРИИ
const studentN = document.querySelector('.student-is-not-in-the-class');
//КНОПКА ОТРИЦАЮЩАЯ НАЛИЧИЕ СТУДЕНТА В АУДИТОРИИ
const studentY = document.querySelector('.student-is-here');
//ЗАГОЛОВОК ВЫВОДЯЩИЙ НА ЭКРАН ИМЯ "ПОБЕДИТЕЛЯ"
const placeAName = document.querySelector('.winners-name');



startBtn.addEventListener('click', (event) => {
    //ПРОВЕРКА БЫЛА ЛИ ИСПОЛЬЗОВАНА КНОПКА, ИЛИ СБРОШЕНА
    if(event.target.dataset.pressed === '1'){
        //СТАРЫЙ ДОБРЫЙ РАНДОМАЙЗЕР
        const randomiz = (amount) => {return Math.floor(Math.random()* (amount));};

        randomNumb = randomiz(whatWeGotLocal.length);
        console.log(randomNumb);

        //ВЫВОДИМ НА ЭКРАН ЭЕЛЕМЕНТ МАССИВА С ИМЕНЕМ "ПОБЕДИТЕЛЯ"
        placeAName.innerText =  whatWeGotLocal[randomNumb];

        //ВЫВОДИ НА ЭКРАН КНОПКИ ПОДТВЕРЖДЕНИЯ
        booleenBtns.style.display = 'flex';
        //СТАВИМ ЧЕКЕР НА СТАРТОВУЮ КНОПКУ
        event.target.dataset.pressed = '2';
    }
});

studentY.addEventListener('click',(event) => {
    //ПРИ УСЛОВИИ ЕСЛИ В МАССИВЕ ОСТАЛСЯ ОДИН ЭЛЕМЕНТ
    if(whatWeGotLocal.length === 1){
        //ОБНОВЛЯЕМ ЛОКАЛ СТОРАДЖ НА ИЗНАЧАЛЬНЫЙ
        localStorage.setItem('what-we-got', JSON.stringify(studentsArray));
        //ПРЯЧЕМ ДИВ С ПОДТВЕРЖДАЮЩИМИ КНОПКАМИ
        booleenBtns.style.display = 'none';
        //СБРАСЫВАЕМ ЧЕКЕР НА СТАРТОВУЮ КНОПКУ
        startBtn.dataset.pressed = '1';
    } else {
        //УДАЛЯЕМ ЭЛЕМЕНТ МАССИВА, С ИМЕНЕМ ВЫБРАННОГО СТУДЕНТА, ИЗ МАССИВА
        whatWeGotLocal.splice(randomNumb, 1);
        //ЗАЛИВАЕМ В ЛОКАЛ ОБНОВЛЕННЫЙ МАССИВ В ВИДЕ СТРОКИ, БЕЗ ТОЛЬКО ЧТО ВЫБРАННОГО СТУДЕНТА
        localStorage.setItem('what-we-got', JSON.stringify(whatWeGotLocal));
        //ПРЯЧЕМ ДИВ С ПОДТВЕРЖДАЮЩИМИ КНОПКАМИ
        booleenBtns.style.display = 'none';
        //СБРАСЫВАЕМ ЧЕКЕР НА СТАРТОВУЮ КНОПКУ
        startBtn.dataset.pressed = '1';
    }
});

studentN.addEventListener('click', (event) => {
    //СБРАСЫВАЕМ ЧЕКЕР НА СТАРТОВУЮ КНОПКУ
    startBtn.dataset.pressed = '1';
    //ПРЯЧЕМ ДИВ С ПОДТВЕРЖДАЮЩИМИ КНОПКАМИ
    booleenBtns.style.display = 'none';
    //УБИРАЕМ ПРЕДЫДУЩЕЕ ИМЯ СТУДЕНТА С ЭКРАНА
    placeAName.innerText =  '';
});

// localStorage.setItem('what-we-got', JSON.stringify(studentsArray));

localCheck();

