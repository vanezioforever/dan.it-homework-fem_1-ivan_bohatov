let burgerMenu = document.querySelector('.top-navigation-menu')
let menu = document.querySelector('.top-navigation')


burgerMenu.addEventListener('click', (event)=>{
    
    if(burgerMenu.dataset.status === "false"){
       burgerMenu.classList = "top-navigation-menu top-navigation-menu-active"
       menu.classList = "top-navigation drop-down"
       burgerMenu.dataset.status = "true"
    } else{
        burgerMenu.classList = "top-navigation-menu"
        menu.classList = "top-navigation"
        burgerMenu.dataset.status = "false"
    }
})