// const images = document.querySelectorAll('.image-to-show');
//
// images.forEach((elem) => elem.style.display ='none');
//
// const stop = document.querySelector('.stop-button');
// const resume = document.querySelector('.resume-button');
//
// let i = 0;
//
// let check = true;
//
//
// const hideSlide = () => {
//   images[i].style.display = 'none';
//     i += 1;
//     if(i === images.length){
//         i = 0;
//     }
//     slideShow();
// };
//
// const slideShow = () => {
//     images[i].style.display = 'block';
//     let slideGo = setTimeout(hideSlide, 1000);
//     stop.addEventListener('click', () => {
//         clearTimeout(slideGo);
//         check = false;});
//     resume.addEventListener('click', () => {
//         if(check === false){slideGo = setTimeout(hideSlide, 1000);
//         check = true;}});
// };
//
// slideShow();

function SlideDizArr ( queryClass, time){
    this.imgArr = document.querySelectorAll(`${queryClass}`);

    this.imgArr.forEach((elem) => elem.style.display ='none');

    const stop = document.createElement('span');
    document.body.appendChild(stop);
    stop.innerText = 'stop it';
    stop.style.cssText = 'height:100;' +
        'width:100;' +
        'background-color:red';
    const resume = document.createElement('span');
    document.body.appendChild(resume);
    resume.innerText = 'resume it';
    resume.style.cssText = 'height:100;' +
        'width:100;' +
        'background-color : green';

    let iCounter = 0;

    let buttonCheck = true;

    console.log(this.imgArr);
    console.log(time);
    console.log(queryClass);

    this.slideShow = () => {
        this.imgArr[iCounter].style.display = 'block';
        let slideGo = setTimeout(hideSlide, time);
        stop.addEventListener('click', () => {
            clearTimeout(slideGo);
            buttonCheck = false;});
        resume.addEventListener('click', () => {
            if(buttonCheck === false){slideGo = setTimeout(hideSlide, time);
            buttonCheck = true;}});
    };

    let hideSlide = () => {
        this.imgArr[iCounter].style.display = 'none';
        iCounter += 1;
        if(iCounter === this.imgArr.length){
            iCounter = 0;
        }
        this.slideShow();
    };
}

let newSlide = new SlideDizArr('.image-to-show',10000);

newSlide.slideShow();