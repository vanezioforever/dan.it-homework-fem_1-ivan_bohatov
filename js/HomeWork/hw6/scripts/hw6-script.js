let arrForFunc = [ 'string', 2, null, false, 'another one', 3, true, [], {}, undefined];
let typeForFilter = prompt('write  type', 'string');

let filterBy = ( arr, type) => {
    return arr.filter(item => typeof item !== type)
};

console.log(filterBy(arrForFunc, typeForFilter));