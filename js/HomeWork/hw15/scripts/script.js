$('.falling-list-item').on('click', function (event) {
    const id = $(event.target).attr('href');
    const destination = $(id).offset().top;
    $('html').animate( { scrollTop: destination}, 1000 );
    return false;
});

$('.scroll-up').on('click', function () {
    $('html').animate( { scrollTop: '0'}, 1000 );
    return false;
});

$(window).on('scroll', function () {
    if($('html').scrollTop() > window.innerHeight){
        $('.scroll-up').show();
    }else{
        $('.scroll-up').hide();
    }
});

$('.hide-section').on('click', function () {
    $('.horiz-gallery').slideToggle(1000);
});