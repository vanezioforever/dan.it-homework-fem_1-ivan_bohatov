const cells = document.querySelectorAll('.field-item');


//рандомайзер для растоновки бомб
const randomiz = () => {
    return  randomNumb = Math.round(Math.random()* (63) );
};

//йункция расставляющая бомбы по карте
const setBomb = () =>
{
    let i = 0;
    while(i < 10){
        const numb = randomiz();
        console.log(numb);
        if (cells[numb].dataset.code !== '1') {
            cells[numb].dataset.code = '1';
            cells[numb].style.backgroundColor = 'yellow';
            i++;
        }
        cells[numb].dataset.code = '1';
        console.log(randomiz());
    }
};


setBomb();

//подсчет отмеченых пустых ячеек
const scoreArr = [];


//запись значения каждого флага
const flagArr = [];


//запись водсчета колва флагов
let flagCount = 0;


//выключает возможность продолжать игру при победе или проигрыше
let winLose = false;


cells.forEach((elem) => {
    elem.addEventListener('click', (event) => {
            //иф для нажатия на бомбу- при котором ты проигрываешь
            if (event.target.dataset.code === '1' && winLose === false) {
                const allBombs = document.querySelectorAll('[data-code="1"]');
                allBombs.forEach((elem) => {
                    elem.style.backgroundColor = 'red';
                });
                const uLose = document.createElement('span');
                document.body.appendChild(uLose);
                uLose.innerText = 'YOU LOOOOOOSEE!!!';
                uLose.style.cssText = 'position: absolute;' +
                    'top : 48%;' +
                    'left : 48%;' +
                    'width : 50px;' +
                    'height : 50px;' +
                    'font-size: 18px;';
                winLose = true;
            }
            //иф для нажатия на пустые ячейки
            else if (event.target.dataset.code !== '1' && event.target.dataset.check !== '1' && winLose === false) {
                event.target.dataset.check = '1';
                event.target.style.backgroundColor = 'blue';
                scoreArr.push(event.target);
                console.log(scoreArr.length);
                let elemIndex = event.target.dataset.index;
                console.log('elemindex' + elemIndex);
                // if(elemIndex === '1' ){
                //     // bombArr.push(document.querySelector([]))
                // }else if(elemIndex === '8'){
                //
                // }else if(elemIndex === '57'){
                //
                // }else if(elemIndex === '64'){
                //
                // }else if(elemIndex > '1' && elemIndex < '8'){
                //
                // }else if(elemIndex > '57' && elemIndex < '64'){
                //
                // }else if(+elemIndex / (9 || 17 || 25 || 33 || 41 || 49) === 1){
                //
                // }else if(+elemIndex / (16 || 24 || 32 || 40 || 48 || 56) === 1){
                //
                // } else {
                //         }
                //     }

            }
            //иф для победы при открытии всех пустых ячеек
            if (scoreArr.length === 54) {
                const uWon = document.createElement('span');
                document.body.appendChild(uWon);
                uWon.innerText = 'YOU WOOON!!!';
                uWon.style.cssText = 'position: absolute;' +
                    'top : 48%;' +
                    'left : 48%;' +
                    'width : 50px;' +
                    'height : 50px;' +
                    'font-size: 18px;';
                winLose = true;
            }
        }
    )
});


//Ивент для правой кнопки мыши
cells.forEach((elem) => {
    elem.addEventListener('contextmenu', (event) => {
        //по условию ячейка не отмечена и флагов меньше 10
        if(event.target.dataset.check !== '1' && winLose === false && flagArr.length <= 10 && event.target.dataset.flag !== '1' && event.target === elem){
            event.preventDefault();
            let flag = document.createElement('p');
            elem.appendChild(flag);
            flag.className = 'event-text';
            flag.innerText = '>';
            flagArr.push(flagCount);
            console.log(flagArr);
            flagCount++;
            event.target.dataset.flag = '1';
        }
        //этот иф убирает флаг если флаг уже поставлен
        else if(event.target.dataset.flag === '1'){
            event.preventDefault();
            elem.removeChild(elem.firstChild);
            flagArr.splice(flagArr.indexOf(flagCount), 1);
            console.log(flagArr);
            flagCount--;
            event.target.dataset.flag = '0';
        } else
            //этот елз просто выключает контекстное меню для красоты
            {
            event.preventDefault();
        }
    })
});