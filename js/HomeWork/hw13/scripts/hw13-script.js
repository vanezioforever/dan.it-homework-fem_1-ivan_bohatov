

//******************L I K E  I N  R E A D M E**************\\
// const colorButton = document.querySelector('.color-changer');
//
// const changeStyle = document.head.querySelector('#main-style');
//
// let localCheck = () => {
//     if (!localStorage.check) {
//         localStorage.setItem('check', 'true');
//     }
// };
//
// let colorRememberSet = () => {
//   if(localStorage.check === 'false'){
//       changeStyle.href = 'css/secondStyle.css';
//   }  else{
//       changeStyle.href = 'css/RamdaStyle.css';
//   }
// };
//
// localCheck();
//
// colorRememberSet();
//
//
// colorButton.addEventListener('click', (event) => {
//     switch (localStorage.check) {
//         case 'true':
//             changeStyle.href = 'css/secondStyle.css';
//             localStorage.check = 'false';
//             break;
//         case 'false':
//             changeStyle.href = 'css/RamdaStyle.css';
//             localStorage.check = 'true';
//             break;
//     }
// });

//************R A N D O M  C O L O R  T H E M E**************************\\

const colorButton = document.querySelector('.color-changer');

const changeStyle = document.head.querySelector('#main-style');

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

let localCheck = () => {
    if (!localStorage.check) {
        localStorage.setItem('check', 'true');
    }
};

let colorRememberSet = () => {
  if(localStorage.check === 'false'){
      document.body.style = `background-color : ${getRandomColor()}; color : ${getRandomColor()}; font-weight : 700; font-style : italic;`;
  }  else{
      document.body.style = '';
  }
};

localCheck();

colorRememberSet();

colorButton.addEventListener('click', (event) => {
        switch (localStorage.check) {
            case 'true':
                document.body.style = `background-color : ${getRandomColor()}; color : ${getRandomColor()}; font-weight : 700; font-style : italic;`;
                localStorage.check = 'false';
                break;
            case 'false':
                document.body.style = '';
                localStorage.check = 'true';
                break;
        }
    });