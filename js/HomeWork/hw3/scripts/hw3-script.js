const userInfo = {
    firstN : prompt("Write first number","10"),
    secondN : prompt("Write your second number","2"),
    whatToDo : prompt("Write what you need to do(+,-,*,/)","/"),
};

function DoSomeMath(userInfo) {
    let theAnswer = 0;
    switch (userInfo.whatToDo) {
        case "+" :
            theAnswer = +userInfo.firstN + +userInfo.secondN;
            break;
        case "-" :
            theAnswer = userInfo.firstN - userInfo.secondN;
            break;
        case "*" :
            theAnswer = userInfo.firstN * userInfo.secondN;
            break;
        case "/" :
            theAnswer = userInfo.firstN / userInfo.secondN;
            break;
    }
    console.log(theAnswer);
}

DoSomeMath(userInfo);