
let inputText = document.getElementById('inputText');

let divFoElements = document.getElementById('divFoElements');

inputText.onblur = (event) => {
    if(+event.target.value > 0){
        if(divFoElements.querySelector('.nonCorrect')){
        divFoElements.querySelector('.nonCorrect').remove()}
        event.target.style.cssText = 'outline: 1px solid green;';

        let spanPrice = document.createElement('span');
        divFoElements.insertBefore(spanPrice, inputText);
        spanPrice.innerText = `Текущая цена : ${event.target.value}`;

        let closeButton = document.createElement('button');
        divFoElements.insertBefore(closeButton, inputText);
        closeButton.innerText = 'X';
        closeButton.style.cssText = 'background-color : red;' +
            'color : white;' +
            'border : none';
        event.target.style.color = 'green';

        closeButton.onclick = (event) => {
            spanPrice.remove();
            event.target.remove();
            inputText.value = ' ';
        }}
    else {
        let alertText = document.createElement('span');
        divFoElements.appendChild(alertText);
        alertText.className = 'nonCorrect';

        alertText.innerText = 'Please enter correct price';
        alertText.style.cssText = 'color : red;' +
            'background-color : black;' +
            'display : inline-block;';

        event.target.style.cssText = 'outline: 1px solid red;';
    }
};