const buttons = document.querySelectorAll('.btn');

window.addEventListener('keydown', (event) => {
    buttons.forEach((elem) => {
        if(elem.innerText === event.key || elem.innerText.toLowerCase() === event.key){
            elem.style.backgroundColor = 'blue';
        } else{ elem.style.backgroundColor = 'black';}
    })
});