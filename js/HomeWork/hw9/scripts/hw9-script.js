const titles = document.querySelectorAll('.tabs-title');
console.dir(titles);
const content = document.querySelectorAll('.content-item');
console.log(content);


func = () => {
    titles.forEach((elem) => {
            elem.addEventListener('click', (event) => {
                const clearActive = document.querySelector('.tabs-title.active');
                clearActive.classList.remove('active');
                const clearActiveContent = document.querySelector('.content-item.active-content');
                clearActiveContent.classList.remove('active-content');

                elem.classList.add('active');

                content.forEach((elemelem) => {
                    if (elemelem.dataset.name === event.currentTarget.dataset.name) {
                        elemelem.classList.add('active-content');
                    }
                })

            })
        }
    );
};

func();