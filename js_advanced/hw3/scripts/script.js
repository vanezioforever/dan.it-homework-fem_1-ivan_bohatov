let table = document.createElement('table');

document.body.appendChild(table);
table.className = 'ogTable';
for (let i = 0; i < 30; i++){
    let tr = document.createElement('tr');
    table.appendChild(tr);

    for(let itd = 0; itd < 30; itd++){
        let td = document.createElement('td');
        td.className = 'og';
        tr.appendChild(td);
    }
}


document.body.addEventListener('click', (event) => {
    let target = event.target;

    if (target.tagName === 'TD') {
        switch (target.className) {
            case 'og' :
                target.className = 'notOg';
                break;
            case 'notOg' :
                target.className = 'og';
                break
        }
    }
    if (target !== table) {
        switch (table.className) {
            case 'ogTable' :
                table.className = 'notOgTable';
                break;
            case 'notOgTable' :
                table.className = 'ogTable';
                break
        }
    }
});