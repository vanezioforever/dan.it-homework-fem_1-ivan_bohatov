
function Hamburger (size, stuffing){
    try{
        if(size === undefined) throw new HamburgerException('invalid size')
        if(stuffing === undefined) throw new HamburgerException('invalid stuffing')

        this.size = size
        this.stuffing = stuffing
        this.topping = [];
    }catch(e){
        alert('error ' + e.message);
    }
}

Hamburger.SIZE_SMALL = {
    type: 'size',
    name: 'small',
    cost : 50,
    cal : 20
};
Hamburger.SIZE_LARGE ={
    type: 'size',
    name: 'large',
    cost : 100,
    cal : 40
};
Hamburger.STUFFING_CHEESE = {
    type: 'stuffing',
    name: 'cheese',
    cost : 10,
    cal : 20
};
Hamburger.STUFFING_SALAD = {
    type: 'stuffing',
    name: 'salad',
    cost : 20,
    cal : 5
};
Hamburger.STUFFING_POTATO = {
    type: 'stuffing',
    name: 'potato',
    cost : 15,
    cal : 10
};
Hamburger.TOPPING_MAYO = {
    cost : 20,
    cal : 5,
    name : 'mayo'
};
Hamburger.TOPPING_SPICE = {
    cost : 15,
    cal : 0,
    name : 'spice'
};

Hamburger.prototype.addTopping = function (topping){
    try{
    if(topping === undefined) throw new HamburgerException('invalid topping for add')
    if(this.topping === undefined) throw new HamburgerException('invalid topping arr')
    if(this.topping.includes(topping)) throw new HamburgerException('There is this topping already')
   
    this.topping.push(topping)
    console.log(`${topping.name} been added`);
    } catch(e){
        alert('error ' + e.message);
    }
}

Hamburger.prototype.removeTopping = function (topping){
    try{
        if (topping === undefined) throw new HamburgerException('invalid topping for remove')
        if (this.topping === undefined) throw new HamburgerException('invalid topping arr')
        if (!this.topping.includes(topping)) throw new HamburgerException('There is no such a topping')

        this.topping.splice(this.topping.indexOf(topping), 1);
        console.log(`${topping.name} been removed`);
    } catch(e){
        alert('error ' + e.message);
    }
}

Hamburger.prototype.getToppings = function (){
    return this.topping.name
}

Hamburger.prototype.getSize = function (){
    return this.size.name
}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing.name
}

Hamburger.prototype.calculatePrice = function () {
    let cost = this.size.cost + this.stuffing.cost;
    this.topping.forEach((elem)=>{
        cost += elem.cost
    })
    return cost
}

Hamburger.prototype.calculateCalories = function () {
    let callories = this.size.cal + this.stuffing.cal;
    this.topping.forEach((elem)=>{
        callories += elem.cal
    })
    return callories
}

function HamburgerException (message) { 
    this.message = message;
 }



 var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE)